# rsync-backup: Simple full & incremental backup

*rsyncb.sh* is a Bash script performing full & incremental backups. It wraps [rsync](https://rsync.samba.org) to backup folders and keep a copy of incremental changes. Optionally, full snapshots from the current copy can be efficiently archived using [SquashFS](http://squashfs.sourceforge.net).

## Download

See [tags](/../tags) page.

## Install

* Install *rsync* and *squashfs-tools*.
* Copy `src/rsyncb.sh` script into a folder such as `/usr/local/bin` (or `$HOME/backup/files` in example script).
* Create the root, current, incremental and full backup folders. For remote backup, create folders and configure automatic SSH login with keys.
* Configure *rsync-backup* (example in *config* folder)
* Configure timer (example in *systemd* folder)

## Configuration

*rsync-backup* configuration is done by defining shell variable in a script imported at *rsync* startup. It is divided in two sections:

 * Global parameters which mainly defines where to store backup files.
 * An array of individual backup definitions.  

Examples of how to configure a local and remote backups of */home* can be found in *config* folder. For remote backup, it's recommended to configure automatic SSH login with keys.

### Global parameters

| Description                              | Example                                    |
| ---------------------------------------- | ------------------------------------------ |
| Backup root folder                       | `path_base="/[your path]"`                 |
| Current full backup folder               | `path_current_backup=$path_base"/current"` |
| Incremental backup folder                | `path_inc_backup=$path_base"/inc"`         |
| Full past snapshot folder                | `path_past_backup=$path_base"/past"`       |
| Minimum path length (for secure removal) | `path_min_length=$((${#path_base} + 2)`    |
| Lock (avoid concurrent backup)           | `path_lock="/tmp/rsyncb_[your name]"`      |
| Time between full backup (day)           | `backup_full_day=6`                        |
| Hour of full backup (hour)               | `backup_full_hour=1`                       |

### Backup entries

Each backup entry requires to add one item in each of these arrays.

| Description                                          | Optional | Example                                            |
| ---------------------------------------------------- | -------- | -------------------------------------------------- |
| Backup name (will be backup folder name)             | No       | `backup_names[$ib]="home"`                         |
| Folder to backup                                     | No       | `backup_sources[$ib]="/home/"`                     |
| Destination prefix (used for remote backup)          | No       | `backup_destinations[$ib]=""`                      |
| Additionnal Rsync options                            | No       | `backup_rsync_options[$ib]='--exclude=.Sync*'`     |
| Do incremental backup ?                              | No       | `backup_do_inc[$ib]=true`                          |
| Remove old incremental backup(s)?                    | No       | `backup_do_inc_cleaning[$ib]=true`                 |
| How long to keep incremental backup before cleaning? | No       | `backup_inc_cleaning_time[$ib]="-120 days"`        |
| Do full snapshot backup?                             | No       | `backup_do_full[$ib]=false`                        |
| Current full backup folder                           | Yes*     | `backup_path_currents[$ib]=$path_base"/current"`   |
| Incremental backup folder                            | Yes*     | `backup_path_incs[$ib]=$path_base"/inc"`           |
| Full past snapshot folder                            | Yes*     | `backup_path_pasts[$ib]=$path_base"/past"`         |

\* Instead of global parameters `path_current_backup`, `path_inc_backup` and `path_past_backup`, these paths can be specified per backup entry. In that case, **all** backups must have an entry in the array.

## Scheduling backup jobs

A *systemd* timer can be used to periodically start *rsync-backup*. Example of required service and timer files can be found in *systemd* folder for */home* example. These two files needs to be copied in systemd config folder (typically `/etc/systemd/system`). The timer then needs to enabled/started:
```
systemctl enable backup_home.timer
```

To simplify the service description *rsync-backup* is started from an separate bash script included in the same *systemd* folder. This script outputs a timed log. Configure the root and log folders in this script.

## License

*rsync-backup* is distributed under the Mozilla Public License Version 2.0 (see /LICENSE).

Copyright (C) 2012-2018 Charles E. Vejnar
