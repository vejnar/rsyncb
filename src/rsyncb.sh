#!/bin/bash

#
# Copyright (C) 2012-2017 Charles E. Vejnar
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

set -o errexit
set -o pipefail

# Parameters
if [ -z "$1" ] ; then
    echo "Missing parameter"
    exit 1
fi
if [ -e "$1" ] ; then
    . "$1"
else
    echo "$1 not found"
    exit 1
fi

if [ -z "$path_lock" ] ; then
    path_lock="/tmp/rsyncb.lock"
fi
if [ -e "$path_lock" ] ; then
    echo "Another backup is running ($path_lock found)"
    exit 1
fi

trap "rm -f $path_lock; exit" INT TERM EXIT
touch $path_lock

# Initialize
now=$(date +%Y%m%d%H)
now_wday=$(date +%u)
now_hour=$(date +%H)
now_month=$(date +%-m)
now_ysemester="$(date +%Y)0$((((now_month-1)/6)+1))"

default_path_current_backup=$path_current_backup
default_path_inc_backup=$path_inc_backup
default_path_past_backup=$path_past_backup

# Do the backup
nbackup=${#backup_sources[@]}
for (( ib = 0 ; ib < nbackup ; ib++ ))
do
    # Backup defaults
    if [ -n "${backup_path_currents[$ib]}" ] ; then
        path_current_backup=${backup_path_currents[$ib]}
    else
        path_current_backup=$default_path_current_backup
    fi
    if [ -n "${backup_path_incs[$ib]}" ] ; then
        path_inc_backup=${backup_path_incs[$ib]}
    else
        path_inc_backup=$default_path_inc_backup
    fi
    if [ -n "${backup_path_pasts[$ib]}" ] ; then
        path_past_backup=${backup_path_pasts[$ib]}
    else
        path_past_backup=$default_path_past_backup
    fi
    # Backup options
    name=${backup_names[$ib]}
    bsource=${backup_sources[$ib]}
    bdestination=${backup_destinations[$ib]}
    rsync_option=${backup_rsync_options[$ib]}

    # Backup
    echo "Backup $name ($bsource --> $bdestination)"
    # Incremental backup
    if ${backup_do_inc[$ib]} ; then
        rsync --archive --delete --delete-excluded --stats --backup --backup-dir=$path_inc_backup/$now_ysemester/$name/$now $rsync_option $bsource $bdestination$path_current_backup/$name
        # Cleaning
        if ${backup_do_inc_cleaning[$ib]} ; then
            echo "Clean old incremental backup"
            for d in $(find $path_inc_backup/*/$name -mindepth 1 -maxdepth 1 -type d) ; do
                if [ $(basename $d) -lt $(date -d "$backup_inc_cleaning_time" +%Y%m%d%H) ] ; then
                    if [ $path_min_length -lt ${#d} ] ; then
                        echo "Deleting $d"
                        rm -fr "$d"
                    fi
                fi
            done
        fi
    fi
    # Full backup
    if ${backup_do_full[$ib]} ; then
        if [ $now_wday -eq $backup_full_day -a $now_hour -eq $backup_full_hour ] ; then
            echo "Get a full copy"
            mksquashfs $path_current_backup/$name $path_past_backup/$name-$now.sqfs -processors 5 -comp gzip -no-progress
        fi
        # Cleaning of full backups
        echo "Clean old full backup"
        monthly_start=$(date -d "-6 week" +%Y%m)
        for (( id = 1 ; id <= 12 ; id++ ))
        do
            monthly_date=$(date -d "${monthly_start}15 -$id month" +%Y%m)
            for f in $(find $path_past_backup -name $name-$monthly_date*.sqfs | sort -n | sed '1d') ; do
                if [ $path_min_length -lt ${#f} ] ; then
                    echo "Deleting $f"
                    rm -f "$f"
                fi
            done
        done
    fi
done

rm -f $path_lock
trap - INT TERM EXIT
