#!/bin/bash

path_base="/data/backup/files"
path_current_backup=$path_base"/current"
path_inc_backup=$path_base"/inc"
path_past_backup=$path_base"/past"
path_min_length=$((${#path_base} + 2))
path_lock="/tmp/rsyncb_home"
backup_full_day=6
backup_full_hour=1

# Local backup of /home folder
ib=0
backup_names[$ib]="home"
backup_sources[$ib]="/home/"
backup_destinations[$ib]=""
backup_rsync_options[$ib]='--exclude=.Sync*'
backup_do_inc[$ib]=true
backup_do_inc_cleaning[$ib]=true
backup_inc_cleaning_time[$ib]="-120 days"
backup_do_full[$ib]=false

# Remote backup of /home on machine "bkm.domain.net"
ib=$(($ib + 1))
backup_names[$ib]="home"
backup_sources[$ib]="/home/"
backup_destinations[$ib]="root@bkm.domain.net:"
backup_rsync_options[$ib]=''
backup_do_inc[$ib]=true
backup_do_inc_cleaning[$ib]=false
backup_inc_cleaning_time[$ib]="-120 days"
backup_do_full[$ib]=false
